module.exports = {
  siteMetadata: {
    title: "Gatsby Default Starter"
  },
  plugins: [
    "gatsby-plugin-react-helmet",
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: `knpvumupg1iz`,
        accessToken: `566d3982f9169bd836cc8a9a6a5c94562025a824ce93ac7e6873546170c9ee2a`,
      },
    },
  ]
};
