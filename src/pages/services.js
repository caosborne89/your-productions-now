import React from "react";
import Link from "gatsby-link";
import Service from "../components/Service";
import Header from "../components/Header/index.js";
import styles from "../assets/css/main.css";
import ServicesMenu from "../components/ServicesMenu"; 

const number = 0;

const ServicesPage = ({data}) => (
  <div>
    <Header />
    <ServicesMenu servicesData={data.allContentfulServiceDetails.edges}/>
  </div>
);

export default ServicesPage;

export const query = graphql`
  query ServicesPageQuery {
    allContentfulServiceDetails (filter: {node_locale: {eq: "en-US"}}, sort: {fields: [order], order: ASC}) {
      edges {
        node {
          title
          serviceDescription {
            id
            internal {
                content
            }
          }
          serviceImage {
            file {
              url
            }
          }
        
        }
      }
    }
  }
`;
