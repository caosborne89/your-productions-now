import React from "react";
import Link from "gatsby-link";
import Header from "../components/Header/index.js";
import CurrentWorkGalleryItem from "../components/CurrentWorkGalleryItem"
import styles from "../assets/css/main.css";

function bannerStyle(img) {
  const bannerStyle = {
    backgroundImage: 'url(' + img + ')',
  }
  return bannerStyle;
};

const WorkPage = ({data}) => (
  <div>
    <Header />
    <h1 className="work-header">Work</h1>
    {data.allContentfulCategoryOfPortfolioVideos.edges.map(({ node }) => (
    <div className="section-portfolio-work">
        <div className="section-title section-title-work">
            <h1 id={node.categoryHeadingId}>{ node.categoryTitle }</h1>
        </div>
            <div className="homepage-gallery-container">
            {node.portfolioVideo.map(function(object, i){
              return <CurrentWorkGalleryItem key={object.id} image={object.videoThumbnailImage.file.url} imgalt={object.imageAlt} description={object.description} link={"/work/" + object.slug}/>
            })}
        </div>
    </div>
    ))}
  </div>
);

export default WorkPage;

export const query = graphql`
  query WorkQuery {
    allContentfulPageBannerImage(filter: {node_locale: {eq: "en-US"}, pageTitle: {eq: "Work"}}) {
      edges {
        node {
          title
          pageTitle
          bannerImage {
            file {
              url
            }
          }
        }
      }
    }
    allContentfulCategoryOfPortfolioVideos (filter: {node_locale: {eq: "en-US"}}, sort: {fields: [order], order: ASC}) {
      edges {
        node {
          id
          categoryTitle
          categoryHeadingId
          portfolioVideo {
            imageAlt
            description
            longDescription {
              id
            }
            thumbnailLink
            videoThumbnailImage {
            file {
                url
              }
            }
            videoTitle
            vimeoSnippet {
              id
            }
            slug
          }
        }
      }
    }
  }
`;
