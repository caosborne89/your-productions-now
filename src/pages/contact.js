import React from "react";
import Link from "gatsby-link";
import Header from "../components/Header/index.js";
import styles from "../assets/css/main.css";

function bannerStyle(img) {
  const bannerStyle = {
    backgroundImage: 'url(' + img + ')',
  }
  return bannerStyle;
};

const ContactPage = ({data}) => (
  <div>
    <Header />
    <div className="all-pages-jumbotron contact-page-jumbotron" style={bannerStyle(data.allContentfulPageBannerImage.edges[0].node.bannerImage.file.url)}/>
    <div className="contact-flex-container">
      <div className="contact-wrapper">
        <h1>Contact Us!</h1>
        <p>{data.allContentfulContactInfoPage.edges[0].node.contactPageIntro.internal.content}</p>
        <div className="contact-info">
          <div className="phone-email">
            <p><b>Phone Number:</b> {data.allContentfulContactInfoPage.edges[0].node.contactInfoForContactPage.phoneNumber}</p>
            <p><b>Email:</b> {data.allContentfulContactInfoPage.edges[0].node.contactInfoForContactPage.email}</p>
          </div>
        </div>
      </div>
      <div className="headshot-container">
        <img src={data.allContentfulContactInfoPage.edges[0].node.headShot.file.url} />
      </div>
    </div>
  </div>
);

export default ContactPage;

export const query = graphql`
  query ContactQuery {
    allContentfulPageBannerImage(filter: {node_locale: {eq: "en-US"}, pageTitle: {eq: "Contact Us"}}) {
      edges {
        node {
          title
          pageTitle
          bannerImage {
            file {
              url
            }
          }
        }
      }
    }
    allContentfulContactInfoPage (filter: {node_locale: {eq: "en-US"}}) {
      edges {
        node {
          title
          contactPageIntro {
            id
            internal {
              type
              mediaType
              content
              contentDigest
              owner
            }
          }
          contactInfoForContactPage{
            contactInformationTitle
            phoneNumber
            addressLine1
            email
            facebookLink
            twitterLink
            instagramLink
          }
          headShot {
            file {
              url
            }
          }
        }
      }
    }
  }
`;
