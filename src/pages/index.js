import React from "react";
import Link from "gatsby-link";
import VideoJumbotron from "../components/VideoJumbotron";
import Service from "../components/Service";
import Testimonial from "../components/Testimonial";
import CurrentWorkGalleryItem from "../components/CurrentWorkGalleryItem";
import CorperateIcon from "../assets/images/services-icons/corperate-icon.svg";
import LifeStyleIcon from "../assets/images/services-icons/lifestyle-videos-icon.svg";
import MusicVideoIcon from "../assets/images/services-icons/music-icon.svg";
import SocialMediaIcon from "../assets/images/services-icons/social-media-icon.svg";
import styles from "../assets/css/main.css";

export default ({ data }) => {
  return (
    <div>
      <VideoJumbotron />
      <div className="content">
        <div className="section-title">
          <h1>Services</h1>
        </div>
        <div className="service-box-container">
          <div className="service-box">
            <Service
              serviceIcon={CorperateIcon}
              serviceIconAlt="Corperate/Commerical Icon"
              serviceDescription="Corporate/Commerical Video Production"
              link="/services"
            />
            <Service
              serviceIcon={LifeStyleIcon}
              serviceIconAlt="Weddings Icon"
              serviceDescription="Lifestyle Videos"
              link="/services"
            />
            <Service
              serviceIcon={MusicVideoIcon}
              serviceIconAlt="Music Videos Icon"
              serviceDescription="Music Videos"
              link="/services"
            />
            <Service
              serviceIcon={SocialMediaIcon}
              serviceIconAlt="Social Media Marketing Icon"
              serviceDescription="Social Media Marketing"
              link="/services"
            />            
          </div>
        </div>
        <div className="section-title">
          <h1>Our Work</h1>
        </div>
        <div className="homepage-gallery-container">
          {data.allContentfulPortfolioVideo.edges.map(({ node }) => (
            <CurrentWorkGalleryItem
            key={node.id}
            image={node.videoThumbnailImage.file.url}
            imgalt={node.imageAlt}
            description={node.description}
            link={"work/" + node.slug}
            />
          ))}
        </div>
        <div className="section-title">
          <h1>Satisfied Clients</h1>
        </div>
        <div className="testimonials-container">
          {data.allContentfulTestimonials.edges.map(({ node }, i) => (
            <Testimonial containerClass={i % 2 == 0 ? "testimonial-container" : "testimonial-container reverse"} image={node.portraitImage.file.url} imgalt="Image of quote author" author={node.fullName + ", " + node.workTitle + " at " + node.company} quote={node.quote.quote} />
          ))}
        </div>
      </div>
    </div>
  );
};

export const query = graphql`
  query IndexQuery {
    allContentfulTestimonials(filter: {node_locale: {eq: "en-US"}, homepage: {eq: true}}) {
      edges {
        node {
          fullName
          homepage
          workTitle
          company
          portraitImage {
            file {
              url
            }
          }
          quote {
            quote
          }
        }
      }
    }

    allContentfulPortfolioVideo
  		(
        filter: 
          {
            node_locale: {eq: "en-US"}
            featured:{eq: true}
            slug:{ne: null}
          }
    	) 
    {
      edges {
        node {
          id
          imageAlt
          description
          thumbnailLink
          videoThumbnailImage {
            file {
              url
            }
          }
          videoTitle
          featured
          slug
        }
      }
    }
  }
`;