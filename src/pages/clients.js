import React from "react";
import Link from "gatsby-link";
import Header from "../components/Header/index.js";
import styles from "../assets/css/main.css";
import CompassLogo from "../assets/images/testimonials-page-imgs/testimonial-logos/Compass Transparent.png";
import RotaryLogo from "../assets/images/testimonials-page-imgs/testimonial-logos/Rotary Club.png";
import MaximLogo from "../assets/images/testimonials-page-imgs/testimonial-logos/Maxim Logo.png";

const number = 0;

function bannerStyle(img) {
  const bannerStyle = {
    backgroundImage: 'url(' + img + ')',
  }
  return bannerStyle;
};

var maximstyle = {
  height: '65px',
};

const ClientsPage = ({data}) => (
  <div>
    <Header />
    <div className="all-pages-jumbotron" style={bannerStyle(data.allContentfulPageBannerImage.edges[0].node.bannerImage.file.url)}/>
    <div className="client-logos-outer-container">
      <h2 className="logo-container-title">Clients that we work with</h2>
      <div className="client-logos-container">
        <a href="#"><img className="client-logo" src={CompassLogo} alt="Compass Logo"/></a>
        <a href="#"><img className="client-logo" src={RotaryLogo} alt="Rotary Club Logo"/></a>
        <a href="#"><img className="client-logo" src={MaximLogo} style={maximstyle} alt="Maxim Logo"/></a>
      </div>
    </div>
    <div className="all-client-testimonials-container">
    {data.allContentfulTestimonials.edges.map(({ node }) => (
      <div className="client-client-outer-container">
        <div className="client-client-container">
          <img className="client-profile-img" src={node.portraitImage.file.url} alt="Client Profile Image"/>
          <div className="client-quote-container">
            <p className="client-quote">{node.quote.quote}</p>
            <b><p className="client-quote-author">{node.fullName + ", " + node.workTitle + " at " + node.company}</p></b>
          </div>
        </div>
      </div>
    ))}
    </div>
</div>
);

export default ClientsPage;

export const query = graphql`
  query ClientsPageQuery {
    allContentfulTestimonials(filter: {node_locale: {eq: "en-US"}, homepage: {eq: false}}) {
      edges {
        node {
          fullName
          homepage
          workTitle
          company
          portraitImage {
            file {
              url
            }
          }
          quote {
            quote
          }
        }
      }
    }

    allContentfulPageBannerImage(filter: {node_locale: {eq: "en-US"}, pageTitle: {eq: "Clients"}}) {
      edges {
        node {
          title
          pageTitle
          bannerImage {
            file {
              url
            }
          }
        }
      }
    }
  }
`;
