import React from "react";
import Link from "gatsby-link";
import Header from "../components/Header/index.js";
import styles from "../assets/css/main.css";

export default ({ data }) => {
  const portfolioVideo = data.allContentfulPortfolioVideo.edges[0].node;
  return (
    <div>
      <Header />
      <div className="portfolio-video-outer-wrapper">
        <div className="portfolio-video-wrapper">
          <h1 className="video-title">{portfolioVideo.videoTitle}</h1>
          <div className="vimeo-wrapper"dangerouslySetInnerHTML={{ __html: portfolioVideo.vimeoSnippet.vimeoSnippet}} />
        </div>
      </div>
    </div>
  );
};

export const query = graphql`
  query PortfolioVideoPageQuery($slug: String!) {
    allContentfulPortfolioVideo (filter: { slug: { eq: $slug } }){
      edges {
        node {
          videoTitle
          description
          longDescription {
            longDescription
          }
          vimeoSnippet {
            vimeoSnippet
          }
        }
      }
    }
  }
`;
