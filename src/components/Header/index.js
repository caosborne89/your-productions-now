import React, { Component } from "react";
import Link from "gatsby-link";
import styles from "./index.css";
import Logo from "../../assets/images/YPN_BLACK_2D.png"

export default class Header extends Component {
  constructor(props) {
    super(props);
    this.dropDownMenu = this.dropDownMenu.bind(this);
    this.highlightPageNav = this.highlightPageNav.bind(this);
    this.state = {
      nav: false
    };
  }

  dropDownMenu = () => {
    const currentState = this.state.nav;
    this.setState({ nav: !currentState });
  };

  highlightPageNav = () => {
    console.log(location.pathname);
  }

  render() {
    return (
      <div className="header-nav-outer-container">
        <div className="header-nav-container">
          <div className="mobile-top-nav-container">
              <a href="/">
              <div className="header-logo">
                <img src={Logo} />
              </div>
              </a>
            <div className="mobile-menu-container" onClick={this.dropDownMenu}>
              <div className="menu-bar1" />
              <div className="menu-bar2" />
              <div className="menu-bar3" />
            </div>
          </div>
          <nav
            className={
              this.state.nav
                ? "navigation mobile show"
                : "navigation mobile hide"
            }
          >
            <Link to="/services/">Services</Link>
            <Link to="/work/" >Work</Link>
            <Link to="/clients/">Clients</Link>
            <Link to="/contact/">Contact</Link>
          </nav>
          <nav className="navigation desktop">
            <Link activeClassName="nav-selected" to="/services/">Services</Link>
            <Link activeClassName="nav-selected" to="/work/">Work</Link>
            <Link activeClassName="nav-selected" to="/clients/">Clients</Link>
            <Link activeClassName="nav-selected" to="/contact/">Contact</Link>
          </nav>
        </div>
      </div>
    );
  }
}