import React, { Component } from "react";
import Link from "gatsby-link";
import styles from "./index.css";
import FB from "./facebook.png";
import Instagram from "./instagram.png";
import Snapchat from "./snapchat.png";
import Twitter from "./twitter.png";
import Youtube from "./youtube.png";
import Linkedin from "./linkedin.png";

export default class Footer extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const contact = this.props.contactInfo;

    return (
      <footer>
        <div className="contact-info">
          <h2>Contact Us</h2>
          <div>
            <p>{contact.phoneNumber}</p>
            <p>{contact.email}</p>
          </div>
        </div>
        <div className="services-list-container">
          <h2>Services</h2>
          <ul className="services-list">
            <li>Video Production</li>
            <li>Post-Production</li>
            <li>Video Editing</li>
            <li>Photography</li>
            <li>Social Media Management</li>
            <li>Social Media Consulting</li>
            <li>Project Management</li>
          </ul>
        </div>
        <div className="social-media-links">
          <h2>Find Us Online</h2>
          <div className="social-media-icons">
            <a href={contact.facebookLink}><img src={FB} /></a>
            <a href={contact.instagramLink}><img src={Instagram} /></a>
            <a href={contact.youtubeLink}><img src={Youtube} /></a>
            <a href={contact.twitterLink}><img src={Twitter} /></a>
            <a href={contact.linkedinLink}><img src={Linkedin} /></a>
          </div>
        </div>
      </footer>
    );
  }
}
