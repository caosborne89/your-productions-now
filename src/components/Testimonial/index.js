import React, { Component } from "react";
import Link from "gatsby-link";
import styles from "./index.css";

export default class Testimonial extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={this.props.containerClass}>
        <div className="testimonial-image">
          <img
            src={this.props.image}
            alt={this.props.imgalt}
          />
        </div>
        <div className="testimonial-quote-container">
          <p className="testimonial-quote">"{this.props.quote}"</p>
          <b><p className="testimonial-quote-author">{this.props.author}</p></b>
        </div>
      </div>
    );
  }
}
