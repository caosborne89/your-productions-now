import React from "react";
import Link from "gatsby-link";
import styles from "./index.css";
import Header from "../Header/index.js";
import CoverImg from "./coverimg.jpeg";

const VideoJumbotron = () => (
  <div className="header-video-container">
    <div className="header-video-inner-container">
      <div className="header-video-background" />
      <Header />
      <img className="header-video" src={CoverImg} alt="Your Productions Now" />
    </div>
    <div className="tag-line-container">
      <div className="jumbotron-buttons">
        <Link to="/work/">Our Work</Link>
      </div>
      <div className="jumbotron-buttons">
        <Link to="/contact/">Contact Us</Link>
      </div>
    </div>
  </div>
);

export default VideoJumbotron;
