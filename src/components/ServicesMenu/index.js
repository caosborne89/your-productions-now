import React, { Component } from "react";
import Link from "gatsby-link";
import styles from "./index.css";

export default class ServicesMenu extends Component {
  constructor(props) {
    super(props);
    this.selectService = this.selectService.bind(this);
    this.state = {
      index: 0
    };
  }

  selectService = num => {
    this.state.index = num;
  };

  render() {
    return (
      <div>
        <nav className="services-nav">
          {this.props.servicesData.map(({ node }, i) => (
              <Link 
                className={
                  this.state.index === i
                  ? "service-nav-link selected"
                  : "service-nav-link"
                } 
                to="#" exact={true} onClick={() => this.selectService(i)}> {node.title}</Link>
            ))}
        </nav>
        <div id="service-description-container">
          <div className="service-image-container">
            <img
              src={this.props.servicesData[this.state.index].node.serviceImage.file.url}
              alt="Service"
              id="service-image"
            />
          </div>
          <p id="service-description">
            {this.props.servicesData[this.state.index].node.serviceDescription.internal.content}
          </p>
        </div>
      </div>
    );
  }
}
