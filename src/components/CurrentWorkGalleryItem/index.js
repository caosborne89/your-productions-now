import React, { Component } from "react";
import Link from "gatsby-link";
import styles from "./index.css";

export default class CurrentWorkGalleryItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="example-gallery-item-container">
        <Link to={this.props.link}>
          <img
            src={this.props.image}
            alt={this.props.imgalt}
            className="example-gallery-image"
          />
          <div className="gallery-item-description-container">
            <div className="text">{this.props.description}</div>
          </div>
        </Link>
      </div>
    );
  }
}
