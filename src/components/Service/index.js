import React, { Component } from "react";
import Link from "gatsby-link";
import styles from "./index.css";

export default class Service extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="service-block">
        <div className="service-description-container">
          <img src={this.props.serviceIcon} alt={this.props.serviceIconAlt} />
          <p>{this.props.serviceDescription}</p>
        </div>
        <Link to={this.props.link}>
          <div className="examples-button">
            <p>More info</p>
          </div>
        </Link>
      </div>
    );
  }
}
