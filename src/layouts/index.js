import React from "react";
import PropTypes from "prop-types";
import Helmet from "react-helmet";
import MainStyle from "../assets/css/main.css";
import Footer from "../components/Footer";

const TemplateWrapper = ({ children,data }) => (
  <div>
    <Helmet
      title="Your Productions Now"
      meta={[
        { name: "description", content: "Sample" },
        { name: "keywords", content: "sample, something" }
      ]}
    />
    <div>{children()}</div>
    <Footer contactInfo={ data.allContentfulContactInfo.edges[0].node }/>
    {console.log(data)}
  </div>
);

TemplateWrapper.propTypes = {
  children: PropTypes.func
};

export default TemplateWrapper;

export const query = graphql`
  query ContactInfoQuery {
    allContentfulContactInfo(filter: {node_locale: {eq: "en-US"}}) {
      edges {
        node {
          id
          contactInformationTitle
          phoneNumber
          addressLine1
          email
          facebookLink
          twitterLink
          instagramLink
          youtubeLink
          linkedinLink
        }
      }
    }
  }
`
