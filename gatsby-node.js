const path = require(`path`);

exports.createPages = ({ graphql, boundActionCreators }) => {
  const { createPage } = boundActionCreators;
  return new Promise((resolve, reject) => {
    graphql(`
      {
        allContentfulPortfolioVideo(
          filter: { node_locale: { eq: "en-US" }, slug: { ne: null } }
        ) {
          edges {
            node {
              slug
            }
          }
        }
      }
    `).then(result => {
      result.data.allContentfulPortfolioVideo.edges.forEach(({ node }) => {
        createPage({
          path: "work/" + node.slug,
          component: path.resolve(`./src/templates/portfolio-video.js`),
          context: {
            // Data passed to context is available in page queries as GraphQL variables.
            slug: node.slug
          }
        });
      });
      resolve();
    });
  });
};

